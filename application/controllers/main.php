<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Gilang
 * Date: 5/10/13
 * Time: 12:38 AM
 * To change this template use File | Settings | File Templates.
 */

class Main extends CI_Controller{
    function __construct(){
        parent::__construct();
    }

    function index(){
        $data=array(
            'title'=>'Home'
        );
        $this->load->view('frontend/element/v_header',$data);
        $this->load->view('frontend/pages/v_main_page');
        $this->load->view('frontend/element/v_footer');
    }
}

