<?php
class Panel extends CI_Controller{
    function __construct(){
        parent::__construct();
    }

    function index(){
        $data=array(
            'title'=>'Login Page'
        );
        $this->load->view('v_panel',$data);
    }
}
