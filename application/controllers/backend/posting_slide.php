<?php
class Posting_slide extends CI_Controller{
    function __construct(){
        parent::__construct();
    }

    function index(){
        $data=array(
            'title'=>'Posting Slide',
            'active_posting' =>'active',
            'active_display_posting' => 'display:block',
            'active_slide' => 'background:#88c4e2'
        );
        $this->load->view('backend/element/v_header',$data);
        $this->load->view('backend/pages/posting/v_slide');
        $this->load->view('backend/element/v_footer');
    }
}
