<?php
class Pesan_keluar extends CI_Controller{
    function __construct(){
        parent::__construct();
    }

    function index(){
        $data=array(
            'title'=>'Pesan Keluar',
            'active_pesan' =>'active',
            'active_display_pesan' => 'display:block',
            'active_pesan_keluar' => 'background:#88c4e2'
        );
        $this->load->view('backend/element/v_header',$data);
        $this->load->view('backend/pages/pesan/v_pesan_keluar');
        $this->load->view('backend/element/v_footer');
    }
}
