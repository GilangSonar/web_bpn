<?php
class Set_webmail extends CI_Controller{
    function __construct(){
        parent::__construct();
    }

    function index(){
        $data=array(
            'title'=>'Pengaturan Webmail',
            'active_pengaturan' =>'active',
            'active_display_pengaturan' => 'display:block',
            'active_webmail' => 'background:#88c4e2'
        );
        $this->load->view('backend/element/v_header',$data);
        $this->load->view('backend/pages/pengaturan/v_webmail');
        $this->load->view('backend/element/v_footer');
    }
}
