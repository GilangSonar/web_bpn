<?php
class Event extends CI_Controller{
    function __construct(){
        parent::__construct();
    }

    function index(){
        $data=array(
            'title'=>'Calendar Event',
            'active_event' =>'active',
        );
        $this->load->view('backend/element/v_header',$data);
        $this->load->view('backend/pages/event/v_event');
        $this->load->view('backend/element/v_footer');
    }
}
