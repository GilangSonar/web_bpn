<?php
class Dashboard extends CI_Controller{
    function __construct(){
        parent::__construct();
    }

    function index(){
        $data=array(
            'title'=>'Dashboard',
            'active_dashboard'=> 'active'
        );
        $this->load->view('backend/element/v_header',$data);
        $this->load->view('backend/pages/dashboard/v_dashboard');
        $this->load->view('backend/element/v_footer');
    }
}
