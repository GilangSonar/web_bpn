<?php
class Pesan_draft extends CI_Controller{
    function __construct(){
        parent::__construct();
    }

    function index(){
        $data=array(
            'title'=>'Draft',
            'active_pesan' =>'active',
            'active_display_pesan' => 'display:block',
            'active_pesan_draft' => 'background:#88c4e2'
        );
        $this->load->view('backend/element/v_header',$data);
        $this->load->view('backend/pages/pesan/v_draft');
        $this->load->view('backend/element/v_footer');
    }
}
