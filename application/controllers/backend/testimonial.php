<?php
class Testimonial extends CI_Controller{
    function __construct(){
        parent::__construct();
    }

    function index(){
        $data=array(
            'title'=>'Testimonial',
            'active_testi' =>'active',
            'active_display_testi' => 'display:block',
            'active_testimonial' => 'background:#88c4e2'
        );
        $this->load->view('backend/element/v_header',$data);
        $this->load->view('backend/pages/testi/v_testimonial');
        $this->load->view('backend/element/v_footer');
    }
}
