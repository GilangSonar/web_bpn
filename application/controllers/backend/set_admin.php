<?php
class Set_admin extends CI_Controller{
    function __construct(){
        parent::__construct();
    }

    function index(){
        $data=array(
            'title'=>'Pengaturan Administrator',
            'active_pengaturan' =>'active',
            'active_display_pengaturan' => 'display:block',
            'active_admin' => 'background:#88c4e2'
        );
        $this->load->view('backend/element/v_header',$data);
        $this->load->view('backend/pages/pengaturan/v_admin');
        $this->load->view('backend/element/v_footer');
    }
}
