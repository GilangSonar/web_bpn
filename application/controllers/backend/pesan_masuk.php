<?php
class Pesan_masuk extends CI_Controller{
    function __construct(){
        parent::__construct();
    }

    function index(){
        $data=array(
            'title'=>'Pesan Masuk',
            'active_pesan' =>'active',
            'active_display_pesan' => 'display:block',
            'active_pesan_masuk' => 'background:#88c4e2'
        );
        $this->load->view('backend/element/v_header',$data);
        $this->load->view('backend/pages/pesan/v_pesan_masuk');
        $this->load->view('backend/element/v_footer');
    }
}
