<?php
class Posting_artikel extends CI_Controller{
    function __construct(){
        parent::__construct();
    }

    function index(){
        $data=array(
            'title'=>'Posting Artikel',
            'active_posting' =>'active',
            'active_display_posting' => 'display:block',
            'active_artikel' => 'background:#88c4e2'
        );
        $this->load->view('backend/element/v_header',$data);
        $this->load->view('backend/pages/posting/v_artikel');
        $this->load->view('backend/element/v_footer');
    }

    function add_artikel(){
        $data=array(
            'title'=>'Add Posting Artikel',
            'active_posting' =>'active',
            'active_display_posting' => 'display:block',
            'active_artikel' => 'background:#88c4e2'
        );
        $this->load->view('backend/element/v_header',$data);
        $this->load->view('backend/pages/posting/v_add_artikel');
        $this->load->view('backend/element/v_footer');
    }
}
