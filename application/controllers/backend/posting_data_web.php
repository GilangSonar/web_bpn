<?php
class Posting_data_web extends CI_Controller{
    function __construct(){
        parent::__construct();
    }

    function index(){
        $data=array(
            'title'=>'Posting Data Website',
            'active_posting' =>'active',
            'active_display_posting' => 'display:block',
            'active_data_web' => 'background:#88c4e2'
        );
        $this->load->view('backend/element/v_header',$data);
        $this->load->view('backend/pages/posting/v_data_web');
        $this->load->view('backend/element/v_footer');
    }
}
