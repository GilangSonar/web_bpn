<!--Top Navbar-->

<div id="navbar" class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

            <div class="nav-collapse collapse pull-left">
                <ul class="nav">
                    <li class="active">
                        <a href="#Home">Home</a>
                    </li>
                    <li>
                        <a href="#profil">Profil</a>
                    </li>
                    <li>
                        <a href="#struktur_pegawai">Struktur Pegawai</a>
                    </li>
                    <li>
                        <a href="#Photo">Photo</a>
                    </li>
                    <li>
                        <a href="#kontak">Contact</a>
                    </li>
                    <li>
                        <a href="#link">Link</a>
                    </li>
                </ul>
            </div>

            <!--Brand-->
            <a class="brand" href="#">BPN<span>WONOSARI</span>
            </a>

        </div>
    </div>
</div>
<div class="container-fluid clearfix">
<div class="row-fluid">
<hr>

<!--Home (welcome slide)-->
<div id="Home" a>
    <div class="container-fluid clearfix" style="padding:0; overflow:hidden">
        <ul class="cb-slideshow">
            <li><span>Image 01</span>

                <div>
                    <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h3>
                </div>
            </li>
            <li><span>Image 02</span>

                <div>
                    <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h3>
                </div>
            </li>
            <li><span>Image 03</span>

                <div>
                    <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h3>
                </div>
            </li>
            <li><span>Image 04</span>

                <div>
                    <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h3>
                </div>
            </li>
            <li><span>Image 05</span>

                <div>
                    <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h3>
                </div>
            </li>
            <li><span>Image 06</span>

                <div>
                    <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h3>
                </div>
            </li>
        </ul>
    </div>
</div>

<!--About-->
<hr>
<div id="profil">
<div class="container-fluid clearfix About">
    <div class="container clearfix">
        <div class="container clearfix TitleSection">

            <h1><span>Tentang</span>BPN<span>Wonosari</span></h1>

        </div>

        <div class="container clearfix">
            <div class="span12">
                <div class="History thumbnails clearfix">
                    <div class="container">

                            <div class="span12">

                                <a href="#myModal" role="button" class="btn" data-toggle="modal">Launch demo modal</a>

                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="container clearfix TitleSection">
                <h1><span>It's Me</span></h1>
            </div>

            <div class="container clearfix">
                <div class="span12">

                    <div class="span4">
                        <div class="Team clearfix">
                            <div class="round-thumb-container">
                                <div class="round-thumb"><span>
<!--                                        <img src="--><?php //echo base_url('asset/images/circlegallery/deri.jpg') ?><!--"></span>-->
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="span4">
                        <div class="Team clearfix">
                            <div class="round-thumb-container">
                                <div class="round-thumb-container">
                                    <div class="round-thumb"><span><img
<!--                                                src="--><?php //echo base_url('asset/images/circlegallery/deri.jpg') ?><!--"></span>-->
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="span4">
                        <div class="Team clearfix">
                            <div class="round-thumb-container">
                                <div class="round-thumb-container">
                                    <div class="round-thumb"><span><img
<!--                                                src="--><?php //echo base_url('asset/images/circlegallery/deri.jpg') ?><!--"></span>-->
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<hr>

<!--Video-->
<div id="struktur_pegawai">
<hr>
<div class="container-fluid clearfix Video">
<div class="container clearfix TitleSection">

    <h1><span>Struktur Pegawai</span></h1>

</div>
    <pre class="prettyprint lang-html" id="list-html" style="display:none"></pre>
    <div class="container clearfix">

<!--slide-->
    <div class="row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <div class="span12">
<!--                    <div class="row-fluid" style="padding: -20px">-->
<!--                        <div class="span12">-->
                            <ul id="org" style="display:none">
                                <li>
                                    Kepala Kantor<br>
                                    <img src="<?php echo base_url('asset/images/user.jpg');?>" class="img-circle"><br>
                                    <p>Ir. Suparto</p>
                                    <ul>
                                        <li id="beer">Kasub TU
                                            <br>
                                            <img src="<?php echo base_url('asset/images/user.jpg');?>" class="img-circle"><br>
                                            <p style="">Gatot Mudiatmoko, sk</p>
                                            <ul>
                                                <li>Kaur Perencanaan & Perencanaan<br>
                                                    <img src="<?php echo base_url('asset/images/user.jpg');?>" class="img-circle"><br>
                                                    <p>aaaaaaaa</p>
                                                </li>
                                                <li>Kaur Umum & Kepegawaian<br>
                                                    <img src="<?php echo base_url('asset/images/user.jpg');?>" class="img-circle"><br>
                                                    <p>aaaaaaa</p>
                                                </li>
                                            </ul>
                                        </li>

                                        <li>Kepala Seksi Hak Tanah & Pendaftaran Tanah<br>
                                            <img src="<?php echo base_url('asset/images/user.jpg');?>" class="img-circle"><br>
                                            <p>Suprasetyo, S.H</p>
                                            <ul>
                                                <li>Kasupsi Penetapan Hak Tanah<br>
                                                    <img src="<?php echo base_url('asset/images/user.jpg');?>" class="img-circle"><br>
                                                    <p>Timbang, S.E</p>
                                                </li>
                                                <li>
                                                    Kasupsi Pengaturan Tanah Pemerintahan<br>
                                                    <img src="<?php echo base_url('asset/images/user.jpg');?>" class="img-circle"><br>
                                                    <p>Sumarno, A.P.Tnh</p>
                                                </li>
                                                <li>
                                                    Kasupsi Pendaftaran Hak<br>
                                                    <img src="<?php echo base_url('asset/images/user.jpg');?>" class="img-circle"><br>
                                                    <p>Bowo Widhiono, A.P.Tnh, S.H</p>
                                                </li>
                                                <li>
                                                    Kasupsi Peralihan, Pembentukan Hak & PTAP<br>
                                                    <img src="<?php echo base_url('asset/images/user.jpg');?>" class="img-circle"><br>
                                                    <p>Bambang Puryono, A.P.Tnh</p>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="fruit">Kepala Seksi Sengketa, Konflik, & Perkara<br>
                                            <img src="<?php echo base_url('asset/images/user.jpg');?>" class="img-circle"><br>
                                            <p>Suharlan S.H</p>
                                            <ul>
                                                <li>Kasupsi Konflik Pertanahan<br>
                                                    <img src="<?php echo base_url('asset/images/user.jpg');?>" class="img-circle"><br>
                                                   <p>Sujoko, A.Pnh, SH</p>
                                                </li>
                                                <li>Kasupsi Perkara Pertanahan<br>
                                                    <img src="<?php echo base_url('asset/images/user.jpg');?>" class="img-circle"><br>
                                                    <p>Suyadi, SH</p>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>Kepala Seksi Pengendalian & Pemberdayaan<br>
                                            <img src="<?php echo base_url('asset/images/user.jpg');?>" class="img-circle"><br>
                                            <p>Ngatiran S.H</p>
                                            <ul>
                                                <li>
                                                    Kasupsi Pengendalian Pertanahan<br>
                                                    <img src="<?php echo base_url('asset/images/user.jpg');?>" class="img-circle"><br>
                                                    <p>Sujoko.A.Pnh, SH</p>
                                                </li>
                                                <li>
                                                    Kasupsi Pemberdayaan Masyarakat<br>
                                                    <img src="<?php echo base_url('asset/images/user.jpg');?>" class="img-circle"><br>
                                                    <p>Wagiman, Aptah</p>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="collapsed">Kepala Seksi Pengaturan & Penataan Pertanahan<br>
                                            <img src="<?php echo base_url('asset/images/user.jpg');?>" class="img-circle"><br>
                                            <p>Sukmono, SIT</p>

                                            <ul>
                                                <li>Kasupsi Penatagunaan Tanah & Kawasan Tertentu<br>
                                                    <img src="<?php echo base_url('asset/images/user.jpg');?>" class="img-circle"><br>
                                                    <p>Timbang, S.E</p>
                                                </li>
                                                <li>Kasupsi Landresorm & Konsolidasi Tanah<br>
                                                    <img src="<?php echo base_url('asset/images/user.jpg');?>" class="img-circle"><br>
                                                    <p>Gatot Suryono, A.P.Tnh</p>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            Kepala Seksi Pengukuran & Pemetaan<br>
                                            <img src="<?php echo base_url('asset/images/user.jpg');?>" class="img-circle"><br>
                                                <p>Suprasetyo, S.H</p>

                                            <ul>

                                                <li>
                                                    Kasupsi Pengukuran & Pemetaan<br>
                                                    <img src="<?php echo base_url('asset/images/user.jpg');?>" class="img-circle"><br>
                                                        <p>Timbang, S.E</p>


                                                </li>
                                                <li>
                                                    Kasupsi Tematik & Potendi Tanah<br>
                                                    <img src="<?php echo base_url('asset/images/user.jpg');?>" class="img-circle"><br>
                                                        <p>Sumarno, A.P.Tnh</p>

                                                </li>


                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            </ul>

                            <div id="chart" class="orgChart"></div>

<!--                        </div>-->
<!---->
<!--                    -->
<!--                </div>-->
            </div>
        </div>
    </div>
</div>
</div>
</div>
<hr>

<!--Photo-->
<div id="Photo">
    <div class="container-fluid clearfix Photo">
        <div class="container clearfix TitleSection">

            <h1>Our <span>Photo</span></h1>

            <h1><span>Lorem</span> ipsum <span>dolor</span> sit <span>amet</span></h1>

        </div>
        <div class="container clearfix">
            <div class="span12">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean nibh erat,
                    sagittis sit amet congue at, aliquam eu libero. Integer molestie, turpis
                    vel ultrices facilisis, nisi mauris sollicitudin mauris.</p>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <nav id="filter"></nav>
                    <section id="container">
                        <ul id="stage">
                            <li data-tags="Fashion">
                                <div class="filter-thumb-container">
                                    <div class="filter-thumb">
                                        <a href="#" data-toggle="modal">
                                            <span><img src="<?php echo base_url('asset/images/1.jpg') ?>"
                                                       alt="Portfolio Filter"/></span>
                                        </a>
                                    </div>
                                </div>

                                <div>
                                    <h4>Lorem ipsum dolor </h4>

                                    <p>Lorem ipsum</p>
                                </div>

                            </li>
                            <li data-tags="Expression,Fashion">
                                <div class="filter-thumb-container">
                                    <div class="filter-thumb">
                                        <a href="#" data-toggle="modal">
                                            <span><img src="<?php echo base_url('asset/images/photo/1.jpg') ?>"
                                                       alt="Portfolio Filter"/></span>
                                        </a>
                                    </div>
                                </div>
                                <div>
                                    <h4>Lorem ipsum dolor </h4>

                                    <p>Lorem ipsum</p>
                                </div>
                            </li>
                            <li data-tags="Wedding,Expression">
                                <div class="filter-thumb-container">
                                    <div class="filter-thumb">
                                        <a href="#" data-toggle="modal">
                                            <span><img src="<?php echo base_url('asset/images/photo/1.jpg') ?>"
                                                       alt="Portfolio Filter"/></span>
                                        </a>
                                    </div>
                                </div>
                                <div>
                                    <h4>Lorem ipsum dolor </h4>

                                    <p>Lorem ipsum</p>
                                </div>
                            </li>
                            <li data-tags="Wedding,Fashion">
                                <div class="filter-thumb-container">
                                    <div class="filter-thumb">
                                        <a href="#" data-toggle="modal">
                                            <span><img src="<?php echo base_url('asset/images/photo/1.jpg') ?>"
                                                       alt="Portfolio Filter"/></span>
                                        </a>
                                    </div>
                                </div>
                                <div>
                                    <h4>Lorem ipsum dolor </h4>

                                    <p>Lorem ipsum</p>
                                </div>
                            </li>
                            <li data-tags="Expression">
                                <div class="filter-thumb-container">
                                    <div class="filter-thumb">
                                        <a href="#" data-toggle="modal">
                                            <span><img src="<?php echo base_url('asset/images/photo/1.jpg') ?>"
                                                       alt="Portfolio Filter"/></span>
                                        </a>
                                    </div>
                                </div>
                                <div>
                                    <h4>Lorem ipsum dolor </h4>

                                    <p>Lorem ipsum</p>
                                </div>
                            </li>
                            <li data-tags="Wedding,Expression,Fashion">
                                <div class="filter-thumb-container">
                                    <div class="filter-thumb">
                                        <a href="#" data-toggle="modal">
                                            <span><img src="<?php echo base_url('asset/images/photo/1.jpg') ?>"
                                                       alt="Portfolio Filter"/></span>
                                        </a>
                                    </div>
                                </div>
                                <div>
                                    <h4>Lorem ipsum dolor </h4>

                                    <p>Lorem ipsum</p>
                                </div>
                            </li>
                            <li data-tags="Expression,Fashion">
                                <div class="filter-thumb-container">
                                    <div class="filter-thumb">
                                        <a href="#" data-toggle="modal">
                                            <span><img src="<?php echo base_url('asset/images/photo/1.jpg') ?>"
                                                       alt="Portfolio Filter"/></span>
                                        </a>
                                    </div>
                                </div>
                                <div>
                                    <h4>Lorem ipsum dolor </h4>

                                    <p>Lorem ipsum</p>
                                </div>
                            </li>
                            <li data-tags="Wedding">
                                <div class="filter-thumb-container">
                                    <div class="filter-thumb">
                                        <a href="#" data-toggle="modal">
                                            <span><img src="<?php echo base_url('asset/images/photo/1.jpg') ?>"
                                                       alt="Portfolio Filter"/></span>
                                        </a>
                                    </div>
                                </div>
                                <div>
                                    <h4>Lorem ipsum dolor </h4>

                                    <p>Lorem ipsum</p>
                                </div>
                            </li>
                            <li data-tags="Jurnalist">
                                <div class="filter-thumb-container">
                                    <div class="filter-thumb">
                                        <a href="#" data-toggle="modal">
                                            <span><img src="<?php echo base_url('asset/images/photo/1.jpg') ?>"
                                                       alt="Portfolio Filter"/></span>
                                        </a>
                                    </div>
                                </div>
                                <div>
                                    <h4>Lorem ipsum dolor </h4>

                                    <p>Lorem ipsum</p>
                                </div>
                            </li>
                        </ul>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
<hr>

<!--Contact-->
<div id="kontak">
    <div class="container-fluid clearfix Contact">
        <div class="container clearfix TitleSection">

            <h1>CONTACT <span>US</span></h1>

            <h1><span>Lorem</span> ipsum <span>dolor</span> sit <span>amet</span></h1>

        </div>
        <div class="container clearfix">
            <div class="container">
                <div class="span8">

                    <!--map-->
                    <h2>Peta Lokasi</h2>

                    <div class="thumbnail">
                        <iframe width="100%" height="574" frameborder="0" scrolling="no" marginheight="0"
                                marginwidth="0"
                                src="https://maps.google.com/maps/ms?msa=0&amp;msid=211850973005555461206.0004d6cb43f65f1fc1cd6&amp;hl=en&amp;ie=UTF8&amp;t=m&amp;ll=-7.95933,110.597434&amp;spn=0.003719,0.00456&amp;z=17&amp;iwloc=0004d6cb4710c53f1f5f0&amp;output=embed"></iframe>
                    </div>
                </div>
                <div class="span4">
                    <h2>Contact Info</h2>

                    <div class="well">
                        <div class="contact-info">
                            <ul>
                                <li><i class="icon-globe" style="margin-right:10px"></i>Lorem ipsum dolor</li>
                                <br/>
                                <li><i class="icon-bullhorn" style="margin-right:10px"></i>+(62) - 8 - 777 - 4323 - 999
                                </li>
                                <br>
                                <li><i class="icon-envelope" style="margin-right:10px"></i> Example@gmail.com</li>
                                <br>
                                <li><i class="icon-map-marker" style="margin-right:10px"></i>Lorem ipsum dolor
                                    consectetuer adipiscing elit.
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="well">
                        <form class="form"
                              id="registerHere" method='post' action=''>
                            <fieldset>
                                <div class="control-group">
                                    <label class="control-label">Name</label>

                                    <div class="controls">
                                        <input type="text" class="input-xlarge" id="user_name" name="user_name">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Email</label>

                                    <div class="controls">
                                        <input type="text" class="input-xlarge" id="user_email" name="user_email">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Your Message</label>

                                    <div class="controls">
                                        <textarea cols="" rows="" class="input-xlarge" id="cpwd" name="cpwd"></textarea>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label"></label>

                                    <div class="controls">
                                        <button type="submit" class="btn" rel="tooltip" title="Send Message">Send
                                            Message
                                        </button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<hr>

<!--Merchandise-->
<div id="link">
    <div class="container-fluid clearfix Merchandise">
        <div class="container clearfix TitleSection">

            <h1><span>LINK</span></h1>



        </div>
        <div class="container clearfix">

            <div class="row-fluid">
                <div class="span6">
                    <div class="ca-menu">
                        <div>
                            <a href="http://www.bpn.go.id/Layanan-Pertanahan.aspx"> <span class="ca-icon">I</span>

                                <div class="ca-content">
                                    <h2 class="ca-main">Layanan Pertanahan Pusat</h2>



                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="span6">
                    <div class="ca-menu">
                        <div>
                            <a href="http://www.presidenri.go.id/"><span class="ca-icon">II</span>

                                <div class="ca-content">
                                    <h2 class="ca-main">Presiden Republik Indonesia</h2>



                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span6">
                    <div class="ca-menu">
                        <div>
                            <a href="http://www.gunungkidulkab.go.id/"> <span class="ca-icon">III</span>

                                <div class="ca-content">
                                    <h2 class="ca-main">Kabupaten gunungkidul</h2>



                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="span6">
                    <div class="ca-menu">
                        <div>
                            <a href="http://www.bpn.go.id/"> <span class="ca-icon">IV</span>

                                <div class="ca-content">
                                    <h2 class="ca-main">BPN Pusat</h2>

                                  

                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span6">
                    <div class="ca-menu">
                        <div>
                            <a href="http://www.setneg.go.id/"> <span class="ca-icon">V</span>

                                <div class="ca-content">
                                    <h2 class="ca-main">Sekretariat Negara</h2>



                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="span6">
                    <div class="ca-menu">
                        <div>
                            <a href="http://www.indonesia.go.id/"> <span class="ca-icon">VI</span>

                                <div class="ca-content">
                                    <h2 class="ca-main">Indonesia</h2>



                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<hr>

<!--Footer-->
<div class="separator"></div>

<div class="container-fluid Footer">
    <div class="container">
        <div class="row">
            <div class="span12 white-text">
                <footer>

                    <!--Social Icons -->
                    <ul class="social-grid">
                        <li>
                            <div class="social-item">
                                <div class="social-info-wrap">
                                    <div class="social-info">
                                        <div class="social-info-front social-twitter">
                                            <a href="http://twitter.com" target="_blank"></a>
                                        </div>
                                        <div class="social-info-back social-twitter-hover">
                                            <a href="http://twitter.com" target="_blank"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="social-item">
                                <div class="social-info-wrap">
                                    <div class="social-info">
                                        <div class="social-info-front social-facebook">
                                            <a href="http://facebook.com" target="_blank"></a>
                                        </div>
                                        <div class="social-info-back social-facebook-hover">
                                            <a href="http://facebook.com" target="_blank"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="social-item">
                                <div class="social-info-wrap">
                                    <div class="social-info">
                                        <div class="social-info-front social-apple">
                                            <a href="http://apple.com" target="_blank"></a>
                                        </div>
                                        <div class="social-info-back social-apple-hover">
                                            <a href="http://apple.com" target="_blank"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="social-item">
                                <div class="social-info-wrap">
                                    <div class="social-info">
                                        <div class="social-info-front social-flickr">
                                            <a href="http://flickr.com" target="_blank"></a>
                                        </div>
                                        <div class="social-info-back social-flickr-hover">
                                            <a href="http://flickr.com" target="_blank"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="social-item">
                                <div class="social-info-wrap">
                                    <div class="social-info">
                                        <div class="social-info-front social-skype">
                                            <a href="http://skype.com" target="_blank"></a>
                                        </div>
                                        <div class="social-info-back social-skype-hover">
                                            <a href="http://skype.com" target="_blank"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="social-item">
                                <div class="social-info-wrap">
                                    <div class="social-info">
                                        <div class="social-info-front social-instagram">
                                            <a href="http://instagram.com" target="_blank"></a>
                                        </div>
                                        <div class="social-info-back social-instagram-hover">
                                            <a href="http://instagram.com" target="_blank"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="social-item">
                                <div class="social-info-wrap">
                                    <div class="social-info">
                                        <div class="social-info-front social-paypal">
                                            <a href="http://paypal.com" target="_blank"></a>
                                        </div>
                                        <div class="social-info-back social-paypal-hover">
                                            <a href="http://paypal.com" target="_blank"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </footer>
            </div>
        </div>

        <br>
        <hr class="soften">

        <!--Copyright-->
        <a href="#welcome" class="brand">Deri<span>Andriana</span></a>
        <a href="#">English</a> -
        <a href="#">Bahasa Indonesia</a>

        <hr class="soften">

        <p>All rights reserved. Theme by &copy;
            <a href="mailto:gilangsonar15@gmail.com">GilangSonar</a>
        </p>

    </div>
</div>
</div>
<!-- /Row -->
</div>
<!-- /container -->

<!-- Modal -->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Modal header</h3>
    </div>
    <div class="modal-body">
        <p>One fine body…</p>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary">Save changes</button>
    </div>
</div>
