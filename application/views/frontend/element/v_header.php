<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title><?php echo $title;?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Photograph - Deri Andriana">
    <meta name="author" content="Deri Andriana">

<!--    CSS-->
    <link rel="stylesheet" href="<?php echo base_url('asset/css/custom.css')?>"/>
    <link rel="stylesheet" href="<?php echo base_url('asset/css/prettify.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('asset/css/jquery.jOrgChart.css')?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/reset.css'); ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/parallax_style_1.1.3.css'); ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/bootstrap.css'); ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/style.css'); ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/about_slider/lean_slider.css'); ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/blur.css'); ?>"/>
    <script src="<?php echo base_url('asset/js/jquery-1.8.2.min.js')?>"></script>

    <script type="text/javascript" src="<?php echo base_url('asset/js/prettify.js');?>"></script>

<!--     Fav and touch icons-->
    <link rel="shortcut icon" href="<?php echo base_url('asset/icon/logo-d.ico')?>">

<!--    Struktur-->
    <script>
        jQuery(document).ready(function() {
            $("#org").jOrgChart({
                chartElement : '#chart',
                dragAndDrop  : true
            });
        });
    </script>
    <script>
        jQuery(document).ready(function() {

            /* Custom jQuery for the example */
            $("#show-list").click(function(e){
                e.preventDefault();

                $('#list-html').toggle('fast', function(){
                    if($(this).is(':visible')){
                        $('#show-list').text('Hide underlying list.');
                        $(".topbar").fadeTo('fast',0.9);
                    }else{
                        $('#show-list').text('Show underlying list.');
                        $(".topbar").fadeTo('fast',1);
                    }
                });
            });

            $('#list-html').text($('#org').html());

            $("#org").bind("DOMSubtreeModified", function() {
                $('#list-html').text('');

                $('#list-html').text($('#org').html());

                prettyPrint();
            });
        });
    </script>
<!--    finish struktur-->

</head>


<body class="clearfix" data-spy="scroll" data-target="#navbar" data-offset="10">