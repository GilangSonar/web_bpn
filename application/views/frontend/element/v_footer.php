<div id="toTop">To Top</div>

<!-- Le javascript==================================================-->
<!-- Placed at the end of the document so the pages load faster -->
<script>
    jQuery(document).ready(function() {

        /* Custom jQuery for the example */
        $("#show-list").click(function(e){
            e.preventDefault();

            $('#list-html').toggle('fast', function(){
                if($(this).is(':visible')){
                    $('#show-list').text('Hide underlying list.');
                    $(".topbar").fadeTo('fast',0.9);
                }else{
                    $('#show-list').text('Show underlying list.');
                    $(".topbar").fadeTo('fast',1);
                }
            });
        });

        $('#list-html').text($('#org').html());

        $("#org").bind("DOMSubtreeModified", function() {
            $('#list-html').text('');

            $('#list-html').text($('#org').html());

            prettyPrint();
        });
    });
</script>
<script src="<?php echo base_url('asset/js/jquery-1.8.2.min.js')?>"></script>
<script src="<?php echo base_url('asset/js/lean-slider.min.js')?>"></script><!-- About Slider-->
<script src="<?php echo base_url('asset/js/my_script.js')?>"></script>
<script src="<?php echo base_url('asset/js/bootstrap.min.js')?>"></script>
<script src="<?php echo base_url('asset/js/jquery.easing.1.3.min.js')?>"></script><!-- parallax-->
<script src="<?php echo base_url('asset/js/jquery.quicksand.min.js')?>"></script>
<script src="<?php echo base_url('asset/js/jquery.validate.min.js')?>"></script>
<script src="<?php echo base_url('asset/js/jquery.parallax-1.1.3.min.js')?>"></script><!--  parallax-->
<script src="<?php echo base_url('asset/js/prettify.js')?>"></script>
<script src="<?php echo base_url('asset/js/jOrgChart.js')?>"></script>

</body>

</html>