<!-- START OF LEFT PANEL -->
<div class="leftpanel">

    <div class="logopanel">
        <h1><a href="#">BPN <span>Company Profile</span></a></h1>
    </div><!--logopanel-->

    <div class="datewidget"><script src="<?php echo base_url('asset/backend/js/clock.js'); ?>" type="text/javascript"></script><span id="clock"></span></div>

    <div class="searchwidget">
        <form action="http://themepixels.com/main/themes/demo/webpage/katniss/results.html" method="post">
            <div class="input-append">
                <input type="text" class="span2 search-query" placeholder="Search here...">
                <button type="submit" class="btn"><span class="icon-search"></span></button>
            </div>
        </form>
    </div><!--searchwidget-->

    <div class="leftmenu">
        <ul class="nav nav-tabs nav-stacked">
            <li class="nav-header">Main Navigation</li>
            <li class="<?php if(isset($active_dashboard)){echo $active_dashboard;} ?>"><a href="<?php echo site_url('backend/dashboard')?>"><i class="icon-align-justify"></i> Dashboard</a></li>

            <li class="dropdown <?php if(isset($active_posting)){echo $active_posting;} ?>"><a href="#"><span class="icon-pencil"></span> Posting</a>
                <ul style="<?php if(isset($active_display_posting)){echo $active_display_posting;} ?>">
                    <li style="<?php if(isset($active_artikel)){echo $active_artikel;} ?>"><a href="<?php echo site_url('backend/posting_artikel')?>">Artikel Website</a></li>
                    <li style="<?php if(isset($active_slide)){echo $active_slide;} ?>"><a href="<?php echo site_url('backend/posting_slide')?>">Slide Banner</a></li>
                    <li style="<?php if(isset($active_data_web)){echo $active_data_web;} ?>"><a href="<?php echo site_url('backend/posting_data_web')?>">Data Website</a></li>
                </ul>
            </li>

            <li class="dropdown <?php if(isset($active_pesan)){echo $active_pesan;} ?>"><a href="#"><span class="icon-envelope"></span> Pesan</a>
                <ul style="<?php if(isset($active_display_pesan)){echo $active_display_pesan;} ?>">
                    <li style="<?php if(isset($active_pesan_masuk)){echo $active_pesan_masuk;} ?>"><a href="<?php echo site_url('backend/pesan_masuk')?>">Pesan Masuk</a></li>
                    <li style="<?php if(isset($active_pesan_keluar)){echo $active_pesan_keluar;} ?>"><a href="<?php echo site_url('backend/pesan_keluar')?>">Pesan Keluar</a></li>
                    <li style="<?php if(isset($active_pesan_draft)){echo $active_pesan_draft;} ?>"><a href="<?php echo site_url('backend/pesan_draft')?>">Draft</a></li>
                </ul>
            </li>

            <li class="dropdown <?php if(isset($active_testi)){echo $active_testi;} ?>"><a href="#"><span class="icon-comment"></span> Testimonial & Pengaduan</a>
                <ul style="<?php if(isset($active_display_testi)){echo $active_display_testi;} ?>">
                    <li style="<?php if(isset($active_testimonial)){echo $active_testimonial;} ?>"><a href="<?php echo site_url('backend/testimonial')?>">Testimonial</a></li>
                    <li style="<?php if(isset($active_pengaduan)){echo $active_pengaduan;} ?>"><a href="<?php echo site_url('backend/pengaduan')?>">Pengaduan</a></li>
                </ul>
            </li>

            <li class="dropdown <?php if(isset($active_pengaturan)){echo $active_pengaturan;} ?>"><a href="#"><span class="icon-cog"></span> Pengaturan</a>
                <ul style="<?php if(isset($active_display_pengaturan)){echo $active_display_pengaturan;} ?>">
                    <li style="<?php if(isset($active_admin)){echo $active_admin;} ?>"><a href="<?php echo site_url('backend/set_admin')?>">Administrator</a></li>
                    <li style="<?php if(isset($active_penulis)){echo $active_penulis;} ?>"><a href="<?php echo site_url('backend/set_penulis')?>">Penulis</a></li>
                    <li style="<?php if(isset($active_tags)){echo $active_tags;} ?>"><a href="<?php echo site_url('backend/set_tags')?>">Tags</a></li>
                    <li style="<?php if(isset($active_webmail)){echo $active_webmail;} ?>"><a href="<?php echo site_url('backend/set_webmail')?>">Webmail</a></li>
                </ul>
            </li>

            <li class="<?php if(isset($active_event)){echo $active_event;} ?>"><a href="<?php echo site_url('backend/event')?>"><i class="icon-calendar"></i> Calendar Event</a></li>

        </ul>
    </div><!--leftmenu-->

</div><!--mainleft-->
<!-- END OF LEFT PANEL -->