<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><?php echo $title ?></title>

<!--    STYLE-->
    <link rel="stylesheet" href="<?php echo base_url('asset/backend/css/style.default.css')?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url('asset/backend/prettify/prettify.css')?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url('asset/backend/css/bootstrap-fileupload.min.css')?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url('asset/backend/css/bootstrap-timepicker.min.css')?>" type="text/css" />

<!--    SCRIPT JS-->
    <script type="text/javascript" src="<?php echo base_url('asset/backend/prettify/prettify.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/backend/js/jquery-1.9.1.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/backend/js/jquery-migrate-1.1.1.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/backend/js/jquery-ui-1.9.2.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/backend/js/jquery.flot.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/backend/js/jquery.flot.resize.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/backend/js/bootstrap.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/backend/js/bootstrap-fileupload.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/backend/js/bootstrap-timepicker.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/backend/js/jquery.uniform.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/backend/js/jquery.dataTables.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/backend/js/jquery.validate.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/backend/js/jquery.tagsinput.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/backend/js/jquery.autogrow-textarea.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/backend/js/charCount.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/backend/js/ui.spinner.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/backend/js/chosen.jquery.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/backend/js/modernizr.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/backend/js/detectizr.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/backend/js/fullcalendar.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/backend/js/jquery.cookie.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/backend/js/custom.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/backend/js/forms.js')?>"></script>

    <script type="text/javascript" src="<?php echo base_url('asset/backend/js/tinymce/tinymce.min.js')?>"></script>
    <script>
        tinymce.init({
            selector: "textarea",
            theme: "modern",
            height: 300,
            plugins: [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "save table contextmenu directionality emoticons template paste textcolor"
            ]
        });
    </script>

    <!--[if lte IE 8]>
        <script language="javascript" type="text/javascript" src="<?php echo base_url('asset/backend/js/excanvas.min.js')?>"></script>
    <![endif]-->


</head>

<body onload="goforit()">