<?php $this->load->view('backend/subelement/v_sidebar')?>

<!-- START OF RIGHT PANEL -->
<div class="rightpanel">

    <?php $this->load->view('backend/subelement/v_topbar')?>

<div class="breadcrumbwidget">

    <ul class="breadcrumb">
        <li><a href="#">Home</a> <span class="divider">/</span></li>
        <li class="active">Dashboard</li>
    </ul>
</div>
<div class="pagetitle">
    <h1>Dashboard</h1> <span>This is a sample description for dashboard page...</span>
</div>

<div class="maincontent">
<div class="contentinner content-dashboard">
<div class="alert alert-info">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>Welcome!</strong> This alert needs your attention, but it's not super important.
</div>

<div class="row-fluid">
<div class="span12">
    <ul class="widgeticons row-fluid">
        <li class="one_fifth"><a href="#"><img src="<?php echo base_url('asset/backend/img/gemicon/calendar.png')?>" alt="" /><span>Events</span></a></li>
        <li class="one_fifth"><a href="#"><img src="<?php echo base_url('asset/backend/img/gemicon/users.png')?>" alt="" /><span>Manage Users</span></a></li>
        <li class="one_fifth"><a href="#"><img src="<?php echo base_url('asset/backend/img/gemicon/settings.png')?>" alt="" /><span>Settings</span></a></li>
        <li class="one_fifth"><a href="#"><img src="<?php echo base_url('asset/backend/img/gemicon/archive.png')?>" alt="" /><span>Archives</span></a></li>
        <li class="one_fifth last"><a href="#"><img src="<?php echo base_url('asset/backend/img/gemicon/notify.png')?>" alt="" /><span>Notifications</span></a></li>
    </ul>

    <h4 class="widgettitle">Site Impressions</h4>
    <div class="widgetcontent">
        <div id="bargraph2" style="height:200px;"></div>
    </div>

    <h4 class="widgettitle nomargin">Events this month</h4>
    <div class="widgetcontent">
        <div id="calendar" class="widgetcalendar"></div>
    </div>

    <h4 class="widgettitle">Recent Articles</h4>
    <div class="widgetcontent">
        <div id="tabs">
            <ul>
                <li><a href="#tabs-1"><span class="icon-forward"></span> Technology</a></li>
                <li><a href="#tabs-2"><span class="icon-eye-open"></span> Entertainment</a></li>
                <li><a href="#tabs-3"><span class="icon-leaf"></span> Fitness &amp; Health</a></li>
            </ul>
            <div id="tabs-1">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Submitted By</th>
                        <th>Date Added</th>
                        <th class="center">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><a href="#"><strong>Excepteur sint occaecat cupidatat non...</strong></a></td>
                        <td><a href="#">admin</a></td>
                        <td>Jan 02, 2013</td>
                        <td class="center"><a href="#" class="btn"><span class="icon-edit"></span> Edit</a></td>
                    </tr>
                    <tr>
                        <td><a href="#"><strong>Sed ut perspiciatis unde omnis iste natus...</strong></a></td>
                        <td><a href="#">themepixels</a></td>
                        <td>Jan 02, 2013</td>
                        <td class="center"><a href="#" class="btn"><span class="icon-edit"></span> Edit</a></td>
                    </tr>
                    <tr>
                        <td><a href="#"><strong>Sed ut perspiciatis unde omnis iste natus</strong></a></td>
                        <td><a href="#">johndoe</a></td>
                        <td>Jan 01, 2013</td>
                        <td class="center"><a href="#" class="btn"><span class="icon-edit"></span> Edit</a></td>
                    </tr>
                    <tr>
                        <td><a href="#"><strong>Quis autem vel eum iure reprehenderi...</strong></a></td>
                        <td><a href="#">amanda</a></td>
                        <td>Jan 01, 2013</td>
                        <td class="center"><a href="#" class="btn"><span class="icon-edit"></span> Edit</a></td>
                    </tr>
                    <tr>
                        <td><a href="#"><strong>Nemo enim ipsam voluptatem quia</strong></a></td>
                        <td><a href="#">mandylane</a></td>
                        <td>Dec 31, 2012</td>
                        <td class="center"><a href="#" class="btn"><span class="icon-edit"></span> Edit</a></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div id="tabs-2">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Submitted By</th>
                        <th>Date Added</th>
                        <th class="center">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><a href="#"><strong>Nemo enim ipsam voluptatem quia</strong></a></td>
                        <td><a href="#">mandylane</a></td>
                        <td>Jan 04, 2012</td>
                        <td class="center"><a href="#" class="btn"><span class="icon-edit"></span> Edit</a></td>
                    </tr>
                    <tr>
                        <td><a href="#"><strong>Excepteur sint occaecat cupidatat non...</strong></a></td>
                        <td><a href="#">admin</a></td>
                        <td>Jan 02, 2013</td>
                        <td class="center"><a href="#" class="btn"><span class="icon-edit"></span> Edit</a></td>
                    </tr>
                    <tr>
                        <td><a href="#"><strong>Sed ut perspiciatis unde omnis iste natus...</strong></a></td>
                        <td><a href="#">themepixels</a></td>
                        <td>Jan 02, 2013</td>
                        <td class="center"><a href="#" class="btn"><span class="icon-edit"></span> Edit</a></td>
                    </tr>
                    <tr>
                        <td><a href="#"><strong>Sed ut perspiciatis unde omnis iste natus</strong></a></td>
                        <td><a href="#">johndoe</a></td>
                        <td>Jan 01, 2013</td>
                        <td class="center"><a href="#" class="btn"><span class="icon-edit"></span> Edit</a></td>
                    </tr>
                    <tr>
                        <td><a href="#"><strong>Quis autem vel eum iure reprehenderi...</strong></a></td>
                        <td><a href="#">amanda</a></td>
                        <td>Jan 01, 2013</td>
                        <td class="center"><a href="#" class="btn"><span class="icon-edit"></span> Edit</a></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div id="tabs-3">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Submitted By</th>
                        <th>Date Added</th>
                        <th class="center">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><a href="#"><strong>Quis autem vel eum iure reprehenderi...</strong></a></td>
                        <td><a href="#">amanda</a></td>
                        <td>Jan 03, 2013</td>
                        <td class="center"><a href="#" class="btn"><span class="icon-edit"></span> Edit</a></td>
                    </tr>
                    <tr>
                        <td><a href="#"><strong>Nemo enim ipsam voluptatem quia</strong></a></td>
                        <td><a href="#">mandylane</a></td>
                        <td>Dec 31, 2012</td>
                        <td class="center"><a href="#" class="btn"><span class="icon-edit"></span> Edit</a></td>
                    </tr>
                    <tr>
                        <td><a href="#"><strong>Excepteur sint occaecat cupidatat non...</strong></a></td>
                        <td><a href="#">admin</a></td>
                        <td>Jan 02, 2013</td>
                        <td class="center"><a href="#" class="btn"><span class="icon-edit"></span> Edit</a></td>
                    </tr>
                    <tr>
                        <td><a href="#"><strong>Sed ut perspiciatis unde omnis iste natus...</strong></a></td>
                        <td><a href="#">themepixels</a></td>
                        <td>Jan 02, 2013</td>
                        <td class="center"><a href="#" class="btn"><span class="icon-edit"></span> Edit</a></td>
                    </tr>
                    <tr>
                        <td><a href="#"><strong>Sed ut perspiciatis unde omnis iste natus</strong></a></td>
                        <td><a href="#">johndoe</a></td>
                        <td>Jan 01, 2013</td>
                        <td class="center">
                            <a href="#" class="btn"><span class="icon-edit"></span> Edit</a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div><!--span12-->

</div><!--row-fluid-->
</div><!--contentinner-->
</div><!--maincontent-->

</div><!--mainright-->
<!-- END OF RIGHT PANEL -->

<div class="clearfix"></div>

</div><!--mainwrapper-->
<script type="text/javascript">
    jQuery(document).ready(function(){

        // bar graph
        var d2 = [];
        for (var i = 1; i <= 15; i += 1)
            d2.push([i, parseInt(Math.random() * 30)]);

        var stack = 0, bars = true, lines = false, steps = false;
        jQuery.plot(jQuery("#bargraph2"), [ d2 ], {
            series: {
                stack: stack,
                lines: { show: lines, fill: true, steps: steps },
                bars: { show: bars, barWidth: 0.6 }
            },
            grid: { hoverable: true, clickable: true, borderColor: '#bbb', borderWidth: 1, labelMargin: 10 },
            colors: ["#06c"]
        });

        // calendar
        jQuery('#calendar').datepicker();


    });
</script>