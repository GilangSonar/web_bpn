<script type='text/javascript'>

    jQuery(document).ready(function() {

        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();

        var calendar = jQuery('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            buttonText: {
                prev: '&laquo;',
                next: '&raquo;',
                prevYear: '&nbsp;&lt;&lt;&nbsp;',
                nextYear: '&nbsp;&gt;&gt;&nbsp;',
                today: 'Hari Ini',
                month: 'Bulan',
                week: 'Minggu',
                day: 'Hari'
            },
            selectable: true,
            selectHelper: true,
            select: function(start, end, allDay) {
                var title = prompt('Event Title:');
                if (title) {
                    calendar.fullCalendar('renderEvent',
                        {
                            title: title,
                            start: start,
                            end: end,
                            allDay: allDay
                        },
                        true // make the event "stick"
                    );
                }
                calendar.fullCalendar('unselect');
            },
            editable: true,
            events: [
                {
                    title: 'All Day Event',
                    start: new Date(y, m, 1)
                },
                {
                    title: 'Long Event',
                    start: new Date(y, m, d-5),
                    end: new Date(y, m, d-2)
                },
                {
                    title: 'Meeting',
                    start: new Date(y, m, d, 10, 30),
                    allDay: false
                },
                {
                    title: 'Lunch',
                    start: new Date(y, m, d, 12, 0),
                    end: new Date(y, m, d, 14, 0),
                    allDay: false
                },
                {
                    title: 'Birthday Party',
                    start: new Date(y, m, d+1, 19, 0),
                    end: new Date(y, m, d+1, 22, 30),
                    allDay: false
                },
                {
                    title: 'Click for Google',
                    start: new Date(y, m, 28),
                    end: new Date(y, m, 29),
                    url: 'http://google.com/'
                }
            ]
        });

    });

</script>

<?php $this->load->view('backend/subelement/v_sidebar')?>

<!-- START OF RIGHT PANEL -->
<div class="rightpanel">

    <?php $this->load->view('backend/subelement/v_topbar')?>

    <div class="breadcrumbwidget">

        <ul class="breadcrumb">
            <li><a href="<?php echo site_url('backend/dashboard')?>">Dashboard</a> <span class="divider">/</span></li>
            <li class="active">Calendar Event</li>
        </ul>
    </div>
    <div class="pagetitle">
        <h1>Calendar Event</h1> <span>Jadwal event yang diadakan</span>
    </div>

    <div class="maincontent">
        <div class="contentinner">
            <div id='calendar'></div>
        </div>
    </div>

</div>
<!-- END OF RIGHT PANEL -->

<div class="clearfix"></div>

</div>
