<?php $this->load->view('backend/subelement/v_sidebar')?>

<!-- START OF RIGHT PANEL -->
<div class="rightpanel">

    <?php $this->load->view('backend/subelement/v_topbar')?>

    <div class="breadcrumbwidget">

        <ul class="breadcrumb">
            <li><a href="<?php echo site_url('backend/dashboard')?>">Dashboard</a> <span class="divider">/</span></li>
            <li class="active">Posting</li> <span class="divider">/</span>
            <li class="active">Artikel Website</li>
        </ul>
    </div>
    <div class="pagetitle">
        <h1>Tambah Posting Artikel</h1> <span>Form pengisian artikel website</span>
    </div>

    <div class="maincontent">
        <div class="contentinner">
            <h4 class="widgettitle nomargin shadowed">Form Pengisian Artikel Website</h4>
            <div class="widgetcontent bordered shadowed nopadding">
                <form id="form1" class="stdform stdform2" method="post" action="#">
                    <p>
                        <label>Penulis</label>
                        <span class="field">
                            <input type="text" name="penulis" id="penulis" class="input-xxlarge uneditable-input" readonly="readonly"/>
                        </span>
                    </p>

                    <p>
                        <label>Judul Artikel</label>
                        <span class="field">
                            <input type="text" name="judul" id="judul" class="input-xxlarge" />
                        </span>
                    </p>

                    <p>
                        <label>Kategori / Tags</label>
                                <span class="field"><select name="selection" id="selection" class="uniformselect">
                                        <option value="">Choose One</option>
                                        <option value="1">Selection One</option>
                                        <option value="2">Selection Two</option>
                                        <option value="3">Selection Three</option>
                                        <option value="4">Selection Four</option>
                                    </select></span>
                    </p>

                    <p>
                        <label>Isi <small>Anda bisa menuliskan isi, konten & deskripsi disini</small></label>
                        <span class="field"><textarea rows="5" name="location" id="location" class="span5"></textarea></span>
                    </p>

                    <p class="stdformbutton">
                        <button class="btn btn-primary">Submit Button</button>
                        <button type="reset" class="btn">Reset Button</button>
                    </p>
                </form>
            </div>
        </div>
    </div>
</div>

</div>
<!-- END OF RIGHT PANEL -->

<div class="clearfix"></div>

</div>
